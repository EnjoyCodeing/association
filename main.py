from flask import Flask
app = Flask(__name__)


@app.route('/apple-app-site-association')
def hello_world():
    try:
        with open('info.txt', 'r') as f:
            return f.read()
    except Exception as e:
        print(e)
    return 'Hello, World!'

@app.route('/')
def default():
    f = open('index.html', 'r')
    content = f.read()
    f.close()
    return content

app.run(port=8090)
